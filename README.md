# updiProgrammer

A low part-count UPDI programmer with a switch for toggling between UPDI and TX/RX passthrough.

<img src="hardware/pcb/model_front.png" height="150px"><img src="hardware/pcb/model_back.png" height="150px">

## Table of Contents
- [updiProgrammer](#updiprogrammer)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [About The Project](#about-the-project)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Ordering and Manufacturing](#ordering-and-manufacturing)
    - [Installation](#installation)
  - [Usage](#usage)
    - [UPDI Programmer](#updi-programmer)
    - [USB to UART (TX/RX) Adapter](#usb-to-uart-txrx-adapter)
    - [Programming an Arduino with Optiboot](#programming-an-arduino-with-optiboot)
  - [Design Notes](#design-notes)
  - [Contributing](#contributing)


## Background
Lately, I have become obsessed with the Microchip's ATtiny series of microcontrollers. Similar to Arduinos, they have communication protocols like UART, SPI, and I2C, as well as peripherals like digital I/O, ADC, and similar amounts of flash memory and SRAM. Unlike Arduinos, they require no external components (like crystal or capacitors), have a wide operating voltage (1.8v-5.5v), have hardware interrupts on every pin, and cost less than 1 dollar per chip! They come in hand-solderable form factors like SOIC-8 and more compact ones like QFN-24. 

One major difference between normal Arduinos and the ATtiny series (based on the tinyAVR core) is that the ATtiny microcontrollers can be programmed using a single wire called UPDI (Universal Programming and Debugging Interface). You could go out and buy an ATMEL ICE for 100+ dollars or you can make your own UPDI programmer out of a simple USB to UART converter like the CH330 or CH340 ICs and a couple of passive components like resistors and capacitors. Enter: this project. 

## About The Project
When looking for USB to UART converters, I came across the CH330N and the CH340E, both of which are small, widely compatible, USB to UART converters. They don't require external crystals (good for keeping part-counts low and for requiring less PCB space), can be powered off the 5v USB line (again, good for keeping part-counts low), and is compatible with both 5v and 3.3v UART simultaneously, making it widely accessible. The CH330N is an SOIC-8 IC that has the advantage of being hand solderable while the CH340E is an MSOP-10 IC that has the advantage of being smaller than the CH330N while compromising on solderability. 

I have built this project for use with Spence Konde's [MegaTinyCore](https://github.com/SpenceKonde/megaTinyCore) project which recently added support for [pymcuprog](https://pypi.org/project/pymcuprog/). This makes it super easy to upload to UPDI-based devices from the Arduino IDE or, if you want to upload your own .hex files, you can do that too. 

What makes this project different is that I have also added a double-pole-double-throw (DPDT) switch on the PCB that allows you to easily switch between the device acting as a UPDI programmer and acting as a simple USB to UART adapter. This way, you can set the switch to UPDI, select the Serial Port for uploading to the device and, once it is done uploading, you can move the switch to the TX/RX side and communicate with the UART of the device for debugging!

## Getting Started
There are a couple of tools that I most commonly use with this device but you can use this device with any UART communication protocol, UPDI device, or even as an Arduino programmer using the TX/RX/RTS/GND pins. I will be walking through how to use this device to program an ATtiny over the UPDI protocol in the Arduino IDE. 

- Looking for how to order your own? Check out the section on [Ordering and Manufacturing](#ordering-and-manufacturing).
- Looking for how to install drivers? Check out the section on [Installation](#installation).
- Looking for how to wire up the device? Check out the section about your specific use-case:
    - [UPDI Programmer](#updi-programmer)
    - [USB to UART (TX/RX) Adapter](#usb-to-uart-txrx-adapter)
    - [Programming an Arduino with Optiboot](#programming-an-arduino-with-optiboot)

### Prerequisites
I am assuming you have some very basic knowledge of microcontrollers, communication protocols, electrical wiring, and computer usage.

### Ordering and Manufacturing
For information on how to order the PCB, check out the [ordering_info](/hardware/pcb/ordering_info.png) screenshot of the parameters I selected while ordering these PCB's from JLCPCB. I will eventually add more information on how to order the PCBs, parts, and order of operations while assembling the device. 

### Installation
Depending on your platform, you might not need to install anything to your computer. I still haven't tested this on macOS 11+ because the current installation drivers require macOS extensions (something that was removed in macOS 11 for security reasons). I also haven't tested the device without installing the extension on macOS but hopefully in the near future, I will revisit this with more information and documentation. 

Here is the link to the CH330N driver [download page](http://www.wch-ic.com/downloads/CH341SER_ZIP.html) (it does say CH340/CH341 on the page but seems to be compatible with the CH330 series as well).

If you need help installing the drivers outside of the included directions for each specific driver, visit Sparkfun's documentation on [How to Install CH340 Drivers](https://learn.sparkfun.com/tutorials/how-to-install-ch340-drivers/all)

## Usage
This device was primarily designed for usage as a UPDI programmer but it can be used for other purposes as well. Check out each of the different use-cases below for different ways to use this device.

### UPDI Programmer
I am ordering these PCB's this week and will be filling out this usage information as soon as they arrive. Here I will explain how to use the device as a UPDI programmer.

Here are a couple of things I'm assuming you have installed and are somewhat familiar with:
- [Arduino IDE](https://www.arduino.cc/en/software)
- [MegaTinyCore](https://github.com/SpenceKonde/megaTinyCore)

### USB to UART (TX/RX) Adapter
I am ordering these PCB's this week and will be filling out this usage information as soon as they arrive. Here I will explain how to use the device as a USB to UART adapter for reading and writing to the TX and RX lines of a microcontroller. 

### Programming an Arduino with Optiboot
I am ordering these PCB's this week and will be filling out this usage information as soon as they arrive. Here I will explain how to use the device as a programmer for Arduino's with [Optiboot](https://github.com/Optiboot/optiboot). 

## Design Notes
Here are a couple of notes on why I made the choices I made while designing this device. This section will most likely be placeholders while I'm testing certain features where I won't make the most optimal design decision (size, part-count, or efficiency) and instead make something easy to debug or test certain features. 

- LED's on TX/RX but not on UPDI: Documentation says UPDI line can't handle powering TX/RX LED's, will know soon how well it works
- Thickness of PCB: Trying 1.6mm, will know soon how well it works
- CH330N vs. CH340E: I know that the CH340E is smaller and has the CTS pin broken out, as well as a TNOW pin for powering an LED activity indicator. Although those are all useful features, I don't think it is worth the difficulty for the wider population to be able to hand-solder the chip. If it takes a couple tries to solder the CH340E, you might as well have soldered multiple CH330N chips. Feel free to redesign this for the CH340E chip yourself if you want the added features!

## Contributing
Feel free to contribute to this project. I welcome you to open issues, make pull requests, or just fork it and add your own features!